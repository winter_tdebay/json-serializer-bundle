<?php

namespace Wizbii\JsonSerializerBundle\Exception;

class DeserializationNotSupportedException extends \RuntimeException implements SerializerException
{
    public function __construct(string $type, ?\Throwable $previous = null)
    {
        parent::__construct(
            "Serializer can't deserialize some data as the type ('$type') does not implement ArraySerializable interface.",
            SerializerException::CODE_DESERIALIZATION_NOT_SUPPORTED,
            $previous
        );
    }
}
