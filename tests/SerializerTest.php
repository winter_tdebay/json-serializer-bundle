<?php

namespace Tests\Wizbii\JsonSerializerBundle;

use PHPUnit\Framework\TestCase;
use Tests\Wizbii\JsonSerializerBundle\Fixture\NotSerializableObject;
use Tests\Wizbii\JsonSerializerBundle\Fixture\SimpleSerializableObject;
use Wizbii\JsonSerializerBundle\Exception\DeserializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Exception\SerializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Serializer;

class SerializerTest extends TestCase
{
    private Serializer $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = new Serializer();
    }

    public function testItCanOnlySerializeObjectOrArrayOfObjects()
    {
        $this->expectException(SerializationNotSupportedException::class);
        $this->serializer->serialize('any non object or array of object');
    }

    public function testItCanOnlySerializeArraySerializableObjects()
    {
        $this->expectException(SerializationNotSupportedException::class);
        $this->serializer->serialize(new NotSerializableObject());
    }

    public function testItCanSerializeAnObject()
    {
        $object = (new SimpleSerializableObject())->setFoo('bar');
        $this->assertThat($this->serializer->serialize($object), $this->equalTo('{"foo":"bar"}'));
    }

    public function testItCanSerializeASetOfObjects()
    {
        $objects = [
            (new SimpleSerializableObject())->setFoo('bar'),
            (new SimpleSerializableObject())->setFoo('baz'),
        ];
        $this->assertThat($this->serializer->serialize($objects), $this->equalTo('[{"foo":"bar"},{"foo":"baz"}]'));
    }

    public function testItCanSerializeAMapOfObjects()
    {
        $objects = [
            'a' => (new SimpleSerializableObject())->setFoo('bar'),
            'b' => (new SimpleSerializableObject())->setFoo('baz'),
        ];
        $this->assertThat($this->serializer->serialize($objects), $this->equalTo('{"a":{"foo":"bar"},"b":{"foo":"baz"}}'));
    }

    public function testItCanOnlyDeserializeArraySerializableObject()
    {
        $this->expectException(DeserializationNotSupportedException::class);
        $this->serializer->deserialize('{"foo":"bar"}', NotSerializableObject::class);
    }

    public function testItCanDeserializeObject()
    {
        $simpleObject = $this->serializer->deserialize('{"foo":"bar"}', SimpleSerializableObject::class);
        $this->assertThat($simpleObject, $this->isInstanceOf(SimpleSerializableObject::class));
        $this->assertThat($simpleObject->getFoo(), $this->equalTo('bar'));
    }

    public function testItCanDeserializeSetOfObjects()
    {
        $simpleObjects = $this->serializer->deserialize('[{"foo":"bar"},{"foo":"baz"}]', 'array<'.SimpleSerializableObject::class.'>');
        $this->assertThat($simpleObjects, $this->countOf(2));
        $this->assertThat($simpleObjects[0], $this->isInstanceOf(SimpleSerializableObject::class));
        $this->assertThat($simpleObjects[0]->getFoo(), $this->equalTo('bar'));
        $this->assertThat($simpleObjects[1], $this->isInstanceOf(SimpleSerializableObject::class));
        $this->assertThat($simpleObjects[1]->getFoo(), $this->equalTo('baz'));
    }

    public function testItCanDeserializeMapOfObjects()
    {
        $simpleObjects = $this->serializer->deserialize('{"a":{"foo":"bar"},"b":{"foo":"baz"}}', 'array<'.SimpleSerializableObject::class.'>');
        $this->assertThat($simpleObjects, $this->countOf(2));
        $this->assertThat($simpleObjects['a'], $this->isInstanceOf(SimpleSerializableObject::class));
        $this->assertThat($simpleObjects['a']->getFoo(), $this->equalTo('bar'));
        $this->assertThat($simpleObjects['b'], $this->isInstanceOf(SimpleSerializableObject::class));
        $this->assertThat($simpleObjects['b']->getFoo(), $this->equalTo('baz'));
    }
}
