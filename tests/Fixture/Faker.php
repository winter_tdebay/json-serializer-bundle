<?php

namespace Tests\Wizbii\JsonSerializerBundle\Fixture;

class Faker
{
    public static function integer(): int
    {
        return 1;
    }

    public static function float(): float
    {
        return 1.0;
    }

    public static function bool(): bool
    {
        return true;
    }

    public static function string(): string
    {
        return 's';
    }

    public static function array(): array
    {
        return [];
    }

    public static function dateTime(): \DateTime
    {
        return new \DateTime();
    }

    public static function dateTimeImmutable(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}
